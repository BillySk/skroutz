export const FETCH_GIPHIES_SUCCESS = 'FETCH_GIPHIES_SUCCESS';
export const FETCH_GIPHIES_ERROR = 'FETCH_GIPHIES_ERROR';
export const FETCH_GIPHIES_OFFSET_SUCCESS = 'FETCH_GIPHIES_OFFSET_SUCCESS';

export const fetchGiphiesSuccessAction = (name, giphies, pagination) => {
    return {
        type: FETCH_GIPHIES_SUCCESS,
        giphies: giphies,
        name: name,
        pagination: pagination
    }
}

export const fetchGiphiesErrorAction = error => {
    return {
        type: FETCH_GIPHIES_ERROR,
        error: error
    }
}